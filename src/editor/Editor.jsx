import React from 'react';
import ReactQuill, {Quill} from 'react-quill'; // ES6
import CustomToolbar from './Toolbar';
import "./Editor.css";
import 'react-quill/dist/quill.snow.css'; // ES6
import BlotFormatter from 'quill-blot-formatter';
import getPlaceholderModule from 'quill-placeholder-module'
import {htmlDocx} from "html-docx-js";



Quill.register('modules/blotFormatter', BlotFormatter);


let AlignStyle = Quill.import('attributors/style/align')
let BackgroundStyle = Quill.import('attributors/style/background')
let ColorStyle = Quill.import('attributors/style/color')
let DirectionStyle = Quill.import('attributors/style/direction')
let FontStyle = Quill.import('attributors/style/font')
let SizeStyle = Quill.import('attributors/style/size')    

Quill.register(AlignStyle, true);
Quill.register(BackgroundStyle, true);
Quill.register(ColorStyle, true);
Quill.register(DirectionStyle, true);
Quill.register(FontStyle, true);
Quill.register(SizeStyle, true);

Quill.register('modules/placeholder', getPlaceholderModule(Quill, {
  className: 'ql-placeholder-content'  // default
}))


var modules =
{
  toolbar: '#toolbar',
  blotFormatter: {},
  placeholder: {
    delimiters: ['{', '}'],  // default
    placeholders: [
      {id: 'foo', label: 'Foo'},
      {id: 'required', label: 'Required', required: true}
    ]
  }
};


function Editor (props) {
    const {text, setText} = props;

    const handleChange = (v) =>{
        console.log(v);
        setText(v);
    }
    return ( 
      <div>
        <CustomToolbar/>
        <ReactQuill id="editor" value={text} onChange={handleChange}  modules={modules} /> 
      </div>  
    )
}

export default Editor;
//formats={formats}
//<div style={{width: 100, height: 200, background: "blue"}}className="my-editing-area"/>

/*
{
  toolbar: [
    [{ 'header': [1, 2, false] }],
    ['bold', 'italic', 'underline','strike', 'blockquote'],
    [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
    ['link', 'image'],
    ['clean'],
    [{"placeholder": ["foo", "required"]}]
  ],
  placeholder: {
    delimiters: ['{', '}'],  // default
    placeholders: [
      {id: 'foo', label: 'Foo'},
      {id: 'required', label: 'Required', required: true}
    ]
  }
};*/