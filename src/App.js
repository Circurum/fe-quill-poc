import React, {useState} from 'react';
import fs from 'fs';
import './App.css';
import Editor from './editor/Editor';
import { jsPDF } from "jspdf";
import htmlDocx from "html-docx-js/dist/html-docx";
import { saveAs } from 'file-saver';
import Blob from 'blob';

function App() {


  const [text, setText] = useState("");
  const convertToPDF = () =>{
    const doc = new jsPDF('p', 'pt', 'letter');
    doc.setFontSize(8);
    var width = 600;
    doc.html('<div style="width: '+width+'px">'+text+'</div>' , {
      callback: function (doc) {
        doc.save("Save");
      },
      x: 10,
      y: 10,
      
   });
  }


  const convertToDocx = () =>{
    var width = 600;
      var html = "<!DOCTYPE html><html><head></head><body>"+text + "</body></html>";
      var conv = htmlDocx.asBlob(html);
      saveAs(conv, 'test.docx');
  }
  
//"<!DOCTYPE html><html><head></head><body>"+text + "</body></html>"


  return (
    <div className="App">
      <header className="App-header">
      <Editor text={text} setText={setText} />
      <button onClick={convertToPDF}>To PDF</button>
      <button onClick={convertToDocx}>To Docx</button>
      </header>
    </div>
  );
}

export default App;
